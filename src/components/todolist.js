import TitleList from "./titlelist"
import React, { useState } from 'react';
import "./todolist.css"
const TodoItem=()=>{
  const todoList = [{
        uniqueNo:1,
        title:"Book the ticket for today evening"
    },{
        uniqueNo:2,
        title:"Rent the movie for tomarrow night"
    },{
        uniqueNo:3,
        title:"Confirm the slot for yoga session tomarrow morning"
    },{
        uniqueNo:4,
        title:"Drop the percal at boomanali"
    },{
        uniqueNo:5,
        title:"Order food from Big Basket"
    },{
        uniqueNo:6,
        title:"Fix the production issue"
    },{
        uniqueNo:7,
        title:"confirm my slot for saturday night"
    },{
        uniqueNo:8,
        title:"Get essentials for sunday car wash"
    }];
    
    const[deleteList,setdeletelist]=useState(todoList)


   const ondeletingItems=(uniqueNo)=>{
        const newTodoList = deleteList.filter((eachItem)=>{
            return eachItem.uniqueNo !==uniqueNo
        })
        setdeletelist(newTodoList);
    }

    return (
        <>
        <div className="samplediv">
        <div className="outerdiv">
            <h1 className="todolist">Simple Todos</h1>
            <div className="titlecontainer">
            {deleteList.map((eachItem)=>{
            return <TitleList eachItem={eachItem}  ondeletingItems={ondeletingItems}/>
            })}
            </div>
            </div>
            </div>
        </>
    )


    

}
export default TodoItem;