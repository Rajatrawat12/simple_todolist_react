import React from 'react';
import "./titlelist.css"
const TitleList=(props)=>{
const {eachItem , ondeletingItems}=props
  const{title,uniqueNo}=eachItem
 

 const onDelete=()=>{
  ondeletingItems(uniqueNo)
  

  }


return( 
    <div className='maincontainer'>
   <div className='bgcontainer'>
    <p className='title'>{title}</p>
    <button className="button" onClick= {onDelete}>Delete</button>
   </div>
   </div>
)

}
export default TitleList;